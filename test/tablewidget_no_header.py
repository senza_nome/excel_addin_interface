'''
Created on 1 Dec 2018

@author: Daniel
'''
import sys
from PyQt4 import QtGui

app = QtGui.QApplication(sys.argv)
combo = QtGui.QComboBox()
combo.addItems(['uno', 'due', 'tre'])
combo2 = QtGui.QComboBox()
combo2.addItems(['uno', 'due', 'tre'])
combo3 = QtGui.QComboBox()
combo3.addItems(['uno', 'due', 'tre'])

class MyWindow(QtGui.QWidget):
    def __init__(self, parent=None):
        super(MyWindow, self).__init__(parent)
 
        self.table = QtGui.QTableWidget(5,5)
        self.table.setCellWidget(0,0, combo)
        self.table.setCellWidget(0,1,combo2)
        self.table.setCellWidget(0,2,combo3)
        self.table.horizontalHeader().setVisible(False)
 
        layout = QtGui.QHBoxLayout()
        layout.addWidget(self.table)
        self.setLayout(layout)

 
 
if __name__ == '__main__':
    
 
    main = MyWindow()
    main.show()
 
    sys.exit(app.exec_())