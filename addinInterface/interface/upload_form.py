# -*- coding: utf-8 -*-
'''
Created on 1 Dec 2018

@author: Daniel
'''
from PySide import QtGui, QtCore
from commonPyqtUtils import pyqtutils
from commonUtils import commonUtils
from .ui.ui_upload import Ui_dialog_upload


class UploadDial(QtGui.QDialog, Ui_dialog_upload):
    
    def __init__(self):
        super(UploadDial, self).__init__()
        self.setupUi(self)
        self.setupInterface()
        
    def setupInterface(self):
        self.label_object.setText('Odoo Object')
        self.comboBox_object.setEditable(True)