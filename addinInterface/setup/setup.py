##############################################################################
#
#    OmniaSolutions, Your own solutions
#    Copyright (C) 28/Ago/2012 OmniaSolutions (<http://www.omniasolutions.eu>). All Rights Reserved
#    info@omniasolutions.eu
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
'''
Created on 07/May/2014

@author: Daniel Smerghetto
'''
import os
from distutils.core import setup
import sys
import py2exe
import psutil
import ctypes
import functools

sys.argv.append('py2exe')

packages=[
          ]

includes= [
           "sip",
           ] 

excludes = [
           ]

dll_excludes = ['MSVCP90.dll']

setup( name="Excel Addin Interface", 
       windows=['../main.py'],
       version="1.0.0",
       description="Excel Addin Interface For Odoo",
       # author, maintainer, contact go here:
       author="Daniel Smerghetto",
       author_email="re_dan@hotmail.it",
       
       options={"py2exe": {
                        'bundle_files': 3, 
                        #'ignores': ignores,
                        'excludes': excludes,
                        'includes': includes,
                        'dll_excludes': dll_excludes,
                        #'dist_dir' : dist_dir,
                        'packages' : packages,
                        #'typelibs' : type_libs,
                        'compressed' : True,}}
       )