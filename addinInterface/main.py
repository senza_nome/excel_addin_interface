# -*- coding: utf-8 -*-
'''
Created on 12 Apr 2018

@author: Daniel
'''
import logging
import os
import tempfile
import commonPyqtUtils
LOG_FILE_NAME = 'flask_excel_interface_log.log'

log_file = os.path.join(tempfile.gettempdir(), LOG_FILE_NAME)
logging.basicConfig(filename=log_file, level=logging.DEBUG)

from OdooQtUi.utils_odoo_conn import utils
import sys
from PySide import QtGui, QtCore
from commonPyqtUtils import pyqtutils
from commonUtils import commonUtils
from functools import partial
from start import MainConnector
from OdooQtUi.RPC.rpc import connectionObj
from addinInterface.interface import upload_form
from excel_connector.excel_connector import ExcelConnector


class AddinInterface(QtCore.QObject):
    
    def __init__(self):
        self.connector = MainConnector()
        self.settings = QtCore.QSettings("ExcelOdoo", "ExcelOdooLayout")
        self.excelApp = self.get_excel_application()
        self.excel_connector = ExcelConnector(self.excelApp)
        self.active_model_dict = {}
        self.active_model_name = ''
        self.odoo_fields_index_rel = {}
        self.fields_definition = {}
        self.tableLines = {}
        self.form_obj = None
        self.getOdooModelsList()
        return super(AddinInterface, self).__init__()
    
    def getOdooModelsList(self):
        fields = ['id', 'name', 'model', 'info']
        filterList = [('transient', '=', False)]
        self.odoo_models = []
        if not connectionObj.userLogged:
            self.dbName, self.username, self.userpass, self.serverIp, self.serverPort, self.scheme, self.connType, self.dbList = utils.loadFromFile()
            self.connector.loginWithUser(self.username, self.userpass, self.dbName, self.serverIp, self.serverPort, self.scheme, self.connType)
            if not connectionObj.userLogged:
                self.odoo_login()
        if connectionObj.userLogged:
            self.odoo_models = connectionObj.readSearch('ir.model', fields, filterList, order='name ASC')
        else:
            pyqtutils.launchMessage('Unable to reach Odoo Server', 'warning')
        return self.odoo_models

    def getOdooFieldsGet(self, model):
        return connectionObj.fieldsGet(model)
    
    def clearFormLayout(self, layout):
        for i in reversed(range(layout.count())): 
            widget = layout.itemAt(i).widget()
            if widget:
                widget.setParent(None)
                widget.deleteLater()
    
    def setupFormView(self):
        self.clearFormLayout(self.main_dialog.verticalLayout_3)
        model = self.active_model_dict.get('model')
        if model:
            widget = QtGui.QWidget()
            mainLay = QtGui.QVBoxLayout()
            self.form_obj = self.connector.initFormViewObj(model)
            self.form_obj._valueChangedExt = self._valueChangedExt
            mainLay.addWidget(self.form_obj)
            self.main_dialog.verticalLayout_3.insertWidget(0, widget)
            widget.setLayout(mainLay)
            widget.setStyleSheet('background-color:#FFFFFF;')
            self.restoreItemGeometry()

    def saveItemsGeometry(self):
        self.settings.setValue("global_window_geometry", self.main_dialog.saveGeometry())
        self.settings.setValue("splitter_1", self.main_dialog.splitter.saveState())
    
    def restoreItemGeometry(self):
        if self.settings.contains("global_window_geometry"):
            self.main_dialog.restoreGeometry(self.settings.value("global_window_geometry"))
        if self.settings.contains("splitter_1"):
            self.main_dialog.splitter.restoreState(self.settings.value("splitter_1"))

    def _valueChangedExt(self, fieldName):
        pass
#         if fieldName:
#             fieldVal = self.form_obj.getFieldValue(fieldName)
#             self.setSelectedLineVals({fieldName: fieldVal})

    def odoo_login(self):
        self.connector.loginWithDial()
    
    def get_excel_application(self):
        excelApplication = commonUtils.dispatchObject("Excel.Application")
        if not excelApplication:
            pyqtutils.launchMessage('Unable to communicate with Excel', 'warning')
            return
        return excelApplication
        
    def populateTable(self, active_sheet, tableWidget):
        if self.excelApp:
            self.tableLines = self.excel_connector.getLines(active_sheet)
            # TODO: Lasciare la prima linea per settare le info
            # Ad esempio per i selection mettere i valori possibili
            # Se trovo la colonna ID vedo di fare la write, sennò faccio la create, informare l'utente su questo comportamento
            pyqtutils.commonPopulateTableSimple(tableWidget, self.tableLines, rowPosition=1, forceColCount=self.excel_connector.maxColCount)
        tableWidget.itemSelectionChanged.connect(self.clickedRow)
        self.setupColumnsComboFields()

    def clickedRow(self):
        self.changedField()

    def setSelectedLineVals(self, valsDict):
        items = self.main_dialog.tableWidget.selectedItems()
        out = {}
        fieldsToWrite = valsDict.keys()
        for item in items:
            rowIndex = item.row()
            for colIndex in range(self.main_dialog.tableWidget.columnCount()):
                fieldName = self.getFieldByColumn(colIndex)
                if fieldName in fieldsToWrite:
                    item = self.main_dialog.tableWidget.item(rowIndex, colIndex)
                    item.setText(str(valsDict[fieldName]))
        return out

    def getSelectedLineVals(self):
        items = self.main_dialog.tableWidget.selectedItems()
        out = {}
        for item in items:
            rowIndex = item.row()
            for colIndex in range(self.main_dialog.tableWidget.columnCount()):
                fieldDict = self.getFieldDictByColumn(colIndex)
                fieldName = fieldDict.get('field_name')
                if fieldName:
                    item = self.main_dialog.tableWidget.item(rowIndex, colIndex)
                    text_val = item.data(0)
                    out[fieldName] = self.parseOutValue(fieldDict, text_val)
        return out

    def parseOutValue(self, fieldDict, fieldVal):
        fieldType = fieldDict.get('type')
        if not fieldType:
            return
        if fieldType == 'char':
            return fieldVal or ''
        elif fieldType == 'date':
            return fieldVal or ''
        elif fieldType == 'datetime':
            return fieldVal or ''
        elif fieldType == 'float':
            try:
                return eval(fieldVal) or 0.0
            except Exception as ex:
                commonUtils.logWarning(ex, 'parseOutValue')
                return 0.0
        elif fieldType == 'integer':
            try:
                return eval(fieldVal) or 0
            except Exception as ex:
                commonUtils.logWarning(ex, 'parseOutValue')
                return 0
        elif fieldType == 'many2one':
            relation = fieldDict.get('relation', '')
            if relation:
                fieldIds = connectionObj.search(relation, [('name', '=ilike', fieldVal)])
                if fieldIds:
                    return fieldIds[0]
            return False
        elif fieldType == 'selection':
            selection = fieldDict.get('selection', '')
            for key, val in selection:
                if fieldVal.lower() == key.lower() or fieldVal.lower() == val.lower() or fieldVal.lower() in val.lower():
                    return key
            return False
        elif fieldType == 'one2many':
            pass
        elif fieldType == 'many2many':
            pass
        
    def getFieldByColumn(self, comboIndex):
        fieldDict = self.getFieldDictByColumn(comboIndex)
        if fieldDict:
            return fieldDict.get('field_name')
        return ''
        
    def getFieldDictByColumn(self, comboIndex):
        cellWidget = self.main_dialog.tableWidget.cellWidget(0, comboIndex)
        if cellWidget:
            currIndex = cellWidget.currentIndex()
            fieldDict = cellWidget._fieldsDict[currIndex]
            if fieldDict:
                return fieldDict
        return {}
        
    def changedField(self, comboTableIndex=None, comboChoiceindex=None):
        comboTableIndex
        comboChoiceindex
        lineVals = self.getSelectedLineVals()
        for fieldName, fieldVal in lineVals.items():
            self.form_obj.setValueField(fieldName, fieldVal)
        
    def check_active_sheet(self):
        active_sheet = self.excel_connector.get_active_sheet()
        if active_sheet:
            return active_sheet
        sheets = self.excel_connector.get_sheets()
        if len(sheets) == 1:
            return sheets.values()[0]
        sheetNames = list(sheets.keys())
        sheetNames.sort()
        choice = pyqtutils.dialogMultyChoice('Select the sheet to import', sheetNames)
        if choice:
            return sheets.get(choice)
    
    def refreshTable(self):
        self.main_dialog.tableWidget.resizeColumnsToContents()
        self.main_dialog.tableWidget.horizontalHeader().setStretchLastSection(True)

    def setupColumnsComboFields(self):
        stringsDict = {}
        for key, elemDict in self.fields_definition.items():
            elemDict['field_name'] = key
            fieldString = elemDict.get('string')
            if fieldString:
                if fieldString in stringsDict:
                    stringsDict[fieldString].append(elemDict)
                else:
                    stringsDict[fieldString] = [elemDict]
        strings = list(stringsDict.keys())
        strings.sort()
            
        for colIndex in range(self.excel_connector.maxColCount):
            combo = QtGui.QComboBox()
            combo.blockSignals(True)
            combo.currentIndexChanged.connect(partial(self.changedField, colIndex))
            combo.addItem('NONE')
            comboDict = {0: {}}
            index = 1
            for stringVal in strings:
                for val in stringsDict.get(stringVal, []):
                    key = val.get('field_name') or ''
                    comboStr = val.get('string') or key
                    relation = val.get('relation', '')
                    comboStr += ' (%s)' % (key)
                    if relation:
                        comboStr += ' [%s]' % (relation)
                    combo.addItem(comboStr)
                    val['field_name'] = key
                    comboDict[index] = val
                    index += 1 
            combo.blockSignals(False)
            combo._fieldsDict = comboDict
            self.main_dialog.tableWidget.setCellWidget(0, colIndex, combo)
        self.refreshTable()

    def changedReferenceModel(self, index):
        modelsDict = self.main_dialog.comboBox_object._modelsDict
        self.active_model_dict = modelsDict.get(index)
        model = self.active_model_dict.get('model')
        if not model:
            self.active_model_name = ''
            logging.warning('Unable to get model from odoo_model_index_rel with index %r and models %r' % (index, self.odoo_model_index_rel))
            return
        self.active_model_name = model
        self.fields_definition = self.getOdooFieldsGet(model)
        self.setupColumnsComboFields()
        self.setupFormView()

    def populateModelCombo(self):
        modelsDict = {}
        self.main_dialog.comboBox_object._modelsDict = {}
        self.main_dialog.comboBox_object.currentIndexChanged.connect(self.changedReferenceModel)
        self.main_dialog.comboBox_object.clear()
        self.main_dialog.comboBox_object.blockSignals(True)
        self.main_dialog.comboBox_object.addItem('NONE')
        modelsDict[0] = {}
        for odooModelDict in self.odoo_models:
            modelName = odooModelDict.get('name', 'NONE')
            modelObj = odooModelDict.get('model', 'NONE')
            name = '%s [%s]' % (modelName, modelObj)
            self.main_dialog.comboBox_object.addItem(name)
            index = self.odoo_models.index(odooModelDict)
            modelsDict[index + 1] = odooModelDict
        self.main_dialog.comboBox_object.blockSignals(False)
        self.main_dialog.comboBox_object._modelsDict = modelsDict

    def splitterMove(self,  val1=False, val2=False):
        val1
        val2
        self.saveItemsGeometry()
        
    def odoo_upload(self):
        if connectionObj.userLogged:
            self.main_dialog = upload_form.UploadDial()
            self.restoreItemGeometry()
            self.main_dialog.splitter.splitterMoved.connect(self.splitterMove)
            active_sheet = self.check_active_sheet()
            if active_sheet:
                self.populateTable(active_sheet, self.main_dialog.tableWidget)
                self.populateModelCombo()
                self.refreshTable()
                if self.main_dialog.exec_() == QtGui.QDialog.Accepted:
                    pass

arguments = sys.argv
commonUtils.logMessage('info', 'File Called with arguments %r' % (arguments))
app = QtGui.QApplication(sys.argv)
app.setQuitOnLastWindowClosed(False)
app.setStyle('plastique')
interface = AddinInterface()

file_path = arguments[0]
func_to_call = ''
listArgs = []
kArgs = {}
if len(arguments) >= 2: func_to_call = arguments[1]
if len(arguments) >= 3: listArgs = eval(arguments[2])
if len(arguments) >= 4: kArgs = eval(arguments[3])
    
if func_to_call:
    func_obj = None
    try:
        func_obj = getattr(interface, func_to_call)
    except Exception as ex:
        commonUtils.logMessage('error', ex)
    if func_obj:
        commonUtils.logMessage('info', 'Call function %s with args %s and kargs %s' % (func_obj, listArgs, kArgs))
        if not listArgs: listArgs = []
        if not kArgs: kArgs = {}
        func_obj(*listArgs, **kArgs)

#app.exec_()

if __name__ == '__main__':
    interface = AddinInterface()
    interface.odoo_upload()
    